package nz.ac.vuw.mcs.fridge.webfridge.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PurchaseView implements View {
	private final Fridge fridge;
	private final AuthenticatedUser user;

	/**
	 * The current order.
	 */
	private List<OrderLine> order = new ArrayList<OrderLine>();

	/**
	 * Cached result of fridge.getStockForUser(user), initially empty to NullPointerExceptions.
	 */
	private Map<String, StockItem> stock = Collections.emptyMap();

	/**
	 * Cached real name of the user.
	 */
	private String userRealname;

	private final Panel purchasePanel = new VerticalPanel();
	private final Label errorLabel = new Label();
	private final Panel mainPanel = new HorizontalPanel();
	private final StockView stockView = new StockView();
	private final Panel orderPanel = new VerticalPanel();
	private final Label userLabel = new Label();
	private final Panel addItemPanel = new HorizontalPanel();
	private final TextBox itemToAddField = new TextBox();
	private final TextBox quantityToAddField = new TextBox();
	private final OrderView orderView = new OrderView();
	private final Button purchaseButton = new Button("Purchase");

	{
		purchasePanel.add(mainPanel);
		mainPanel.add(stockView.getWidget());
		mainPanel.add(orderPanel);
		orderPanel.add(userLabel);
		orderPanel.add(addItemPanel);
		orderPanel.add(errorLabel);
		errorLabel.addStyleName("error");
		addItemPanel.add(itemToAddField);
		addItemPanel.add(new Label("X"));
		addItemPanel.add(quantityToAddField);
		quantityToAddField.setMaxLength(2);
		quantityToAddField.setValue("1");
		quantityToAddField.setTitle("Quantity");
		orderPanel.add(orderView.getWidget());
		orderPanel.add(purchaseButton);

		//If enter is pressed in the item or quantity fields, add that item to the order
		KeyUpHandler addItemHandler = new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					errorLabel.setText("");
					//TODO: Submit the purchase if the field is empty?
					//Add the item to the order if possible
					try {
						if (addItem(itemToAddField.getText(), Integer.parseInt(quantityToAddField.getText()))) {
							//Success, so reset fields to default values
							itemToAddField.setText("");
							quantityToAddField.setText("1");
						}
						//Whether or not we succeeded, move focus back to item field ready for next item or to correct this one
						itemToAddField.setFocus(true);
						itemToAddField.selectAll();
					}
					catch (NumberFormatException ex) {
						errorLabel.setText("Invalid quantity");
						quantityToAddField.setFocus(true);
						quantityToAddField.selectAll();
					}
				}
			}
		};
		itemToAddField.addKeyUpHandler(addItemHandler);
		quantityToAddField.addKeyUpHandler(addItemHandler);
		//TODO: Filter product list as the user types.
		//TODO: Add items when the user clicks on them.

		purchaseButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (order.size() == 0) {
					errorLabel.setText("Add some items to the order first.");
				}
				else {
					user.purchase(fridge, order, new AsyncCallback<Integer>() {
						@Override
						public void onFailure(Throwable caught) {
							errorLabel.setText(caught.getLocalizedMessage());
						}

						@Override
						public void onSuccess(Integer result) {
							errorLabel.setText("Purchased for " + Fridge.formatMoney(result));
							order.clear();
							refreshUserInfo();
							refreshStock();
							orderView.setData(order);
						}
					});
				}
			}
		});
	}

	public PurchaseView(Fridge fridge, AuthenticatedUser user) {
		this.fridge = fridge;
		this.user = user;
		userLabel.setText(user.getUsername());
	}

	/**
	 * Try to add the item with the given code to the order.
	 * @param productCode The product code to add. This will be converted to upper case.
	 * @param quantity The number of items to add
	 * @return true iff we successfully added the item to the order.
	 */
	private boolean addItem(String productCode, int quantity) {
		//TODO: Search for matches by description substring
		if (quantity == 0) {
			//No point adding nothing of something
			return false;
		}
		productCode = productCode.toUpperCase();
		StockItem product = stock.get(productCode);
		if (product == null) {
			//No such product
			return false;
		}
		updateOrAdd: {
			//If the product is already in the order, update it
			for (OrderLine line: order) {
				if (line.getProduct().productCode.equals(productCode)) {
					line.incrementQty(quantity);
					if (line.getQty() == 0) {
						//Remove the line if it the quantity is now zero
						order.remove(line);
					}
					break updateOrAdd;
				}
			}
			//Otherwise just add it
			order.add(new OrderLine(product, quantity));
		}
		//Update the order view
		orderView.setData(order);
		return true;
	}

	@Override
	public void show() {
		RootPanel.get("purchaseScreen").add(purchasePanel);

		// Focus the cursor on the product name field when the app loads
		itemToAddField.setFocus(true);
		itemToAddField.selectAll();

		user.getRealName(new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				errorLabel.setText("Error getting real name: " + caught.getLocalizedMessage());
			}

			@Override
			public void onSuccess(String realname) {
				userRealname = realname;
				refreshUserInfo();
			}
		});

		refreshStock();
	}

	private void refreshUserInfo() {
		userLabel.setText(userRealname + " (" + user.getUsername() + "): " + Fridge.formatMoney(user.getBalance()));
	}

	private void refreshStock() {
		fridge.getStockForUser(user, new AsyncCallback<Map<String,StockItem>>() {
			@Override
			public void onFailure(Throwable caught) {
				errorLabel.setText("Error getting stock list: " + caught.getLocalizedMessage());
			}

			@Override
			public void onSuccess(Map<String, StockItem> result) {
				stock = result;
				stockView.setData(result);
			}
		});
	}
}
