package nz.ac.vuw.mcs.fridge.webfridge.client;

import nz.ac.vuw.mcs.fridge.backend.Fridge;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class WebFridge implements EntryPoint {
	private Fridge fridge;

	private final Label statusLabel = new Label();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootPanel.get("statusContainer").add(statusLabel);

		Fridge.makeFridge(Config.FRIDGE_SERVER, new AsyncCallback<Fridge>() {
			@Override
			public void onFailure(Throwable caught) {
				statusLabel.setText("Error connecting to fridge server: " + caught.getLocalizedMessage());
				Window.alert("Error connecting to fridge server: " + caught.getLocalizedMessage());
			}

			@Override
			public void onSuccess(Fridge result) {
				fridge = result;
				statusLabel.setText("Connected to fridge server.");
				LoginView loginView = new LoginView(fridge);
				loginView.show();
			}
		});
		statusLabel.setText("Connecting...");
	}
}
