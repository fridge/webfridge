package nz.ac.vuw.mcs.fridge.webfridge.client;

import java.util.List;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 * A view for displaying the items in an order in a table.
 * @author andrew
 */
public class OrderView implements View {
	//Widgets
	private final FlexTable orderTable = new FlexTable();

	{
		orderTable.setText(0, 0, "Code");
		orderTable.setText(0, 1, "Product");
		orderTable.setText(0, 2, "Price");
		orderTable.setText(0, 3, "Quantity");
	}

	public void setData(List<OrderLine> order) {
		int row = 1;
		for (OrderLine item: order) {
			StockItem product = item.getProduct();
			orderTable.setText(row, 0, product.productCode);
			orderTable.setText(row, 1, product.description);
			orderTable.setText(row, 2, Fridge.formatMoney(product.price));
			orderTable.setText(row, 3, Integer.toString(item.getQty()));
			++row;
		}
		//Remove excess rows
		while (row < orderTable.getRowCount()) {
			orderTable.removeRow(row);
		}
	}

	@Override
	public void show() {
	}

	public Widget getWidget() {
		return orderTable;
	}
}
