package nz.ac.vuw.mcs.fridge.webfridge.client;

public interface View {
	public void show();
}
