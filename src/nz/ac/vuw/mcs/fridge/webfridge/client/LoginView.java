package nz.ac.vuw.mcs.fridge.webfridge.client;

import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class LoginView implements View {
	private final Fridge fridge;

	//Widgets
	private final Label errorLabel = new Label();
	private final TextBox nameField = new TextBox();
	private final StockView stockView = new StockView();

	{
		final Button loginButton = new Button("Login");
		final PasswordTextBox passwordField = new PasswordTextBox();

		// We can add style names to widgets
		loginButton.addStyleName("sendButton");
		
		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("nameFieldContainer").add(nameField);
		RootPanel.get("passwordFieldContainer").add(passwordField);
		RootPanel.get("loginButtonContainer").add(loginButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("stockListContainer").add(stockView.getWidget());

		RootPanel.get("purchaseScreen").setVisible(true);

		//If enter is pressed in the username field, focus and select the password field
		nameField.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					passwordField.setFocus(true);
					passwordField.selectAll();
				}
			}
		});

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				login();
			}
			
			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					login();
				}
			}
			
			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void login() {
				System.out.println("login() called");
				// First, we validate the input.
				errorLabel.setText("");
				String username = nameField.getText();
				String password = passwordField.getText();
				if (username.length() == 0) {
					errorLabel.setText("Please enter your username to login.");
					return;
				}
				
				// Then, we try to login
				loginButton.setEnabled(false);
				fridge.parseAuthenticateUser(username, password, new AsyncCallback<AuthenticatedUser>() {
					@Override
					public void onFailure(Throwable caught) {
						errorLabel.setText("Login failure: " + caught.getLocalizedMessage());
					}

					@Override
					public void onSuccess(AuthenticatedUser user) {
						System.out.println("Logged in as " + user.getUsername());
						//Hide the login view
						RootPanel.get("loginScreen").setVisible(false);
						
						System.out.println("Hidden...");

						//Construct purchase view for the logged-in user and show it
						PurchaseView purchaseView = new PurchaseView(fridge, user);
						System.out.println("constructed...");
						purchaseView.show();
						System.out.println("shown");
					}
				});
				errorLabel.setText("Logging in as " + username + "...");
			}
		}
		
		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		loginButton.addClickHandler(handler);
		passwordField.addKeyUpHandler(handler);
	}

	public LoginView(Fridge fridge) {
		this.fridge = fridge;
	}

	public void show() {
		// Focus the cursor on the name field when the app loads
		nameField.setFocus(true);
		nameField.selectAll();

		fridge.getStock(new AsyncCallback<Map<String,StockItem>>() {
			@Override
			public void onFailure(Throwable caught) {
				errorLabel.setText("Error getting stock list: " + caught.getLocalizedMessage());
			}

			@Override
			public void onSuccess(Map<String, StockItem> result) {
				System.out.println(result);
				stockView.setData(result);
			}
		});
	}
}
