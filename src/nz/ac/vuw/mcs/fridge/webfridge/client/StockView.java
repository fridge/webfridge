package nz.ac.vuw.mcs.fridge.webfridge.client;

import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

public class StockView implements View {
	//Widgets
	private final FlexTable stockTable = new FlexTable();

	{
		stockTable.setText(0, 0, "Code");
		stockTable.setText(0, 1, "Product");
		stockTable.setText(0, 2, "Price");
		stockTable.setText(0, 3, "Quantity in stock");
	}

	public void setData(Map<String, StockItem> stock) {
		int row = 1;
		for (StockItem item: stock.values()) {
			stockTable.setText(row, 0, item.productCode);
			stockTable.setText(row, 1, item.description);
			stockTable.setText(row, 2, Fridge.formatMoney(item.price));
			stockTable.setText(row, 3, Integer.toString(item.inStock));
			if (item.inStock < 0) {
				stockTable.getRowFormatter().addStyleName(row, "negativeStock");
			}
			else if (item.inStock == 0) {
				stockTable.getRowFormatter().addStyleName(row, "zeroStock");
			}
			++row;
		}
		//Remove excess rows
		while (row < stockTable.getRowCount()) {
			stockTable.removeRow(row);
		}
	}

	@Override
	public void show() {
	}

	public Widget getWidget() {
		return stockTable;
	}
}
