package nz.ac.vuw.mcs.fridge.backend.util;

public class Crypt {
	private static final String NONCE_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final int NONCE_LENGTH = 20;

	public static String hex(byte[] array) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; ++i) {
			sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).toUpperCase().substring(1,3));
		}
		return sb.toString();
	}

	public static native String md5(String message) /*-{
		return $wnd.Crypto.MD5(message);
	}-*/;

	/**
	 * Given a shared secret and a message, calculate the HMAC-MD5 for the message and convert it the hexadecimal.
	 * @param secret The shared secret.
	 * @param message The message.
	 * @return The HMAC-MD5 in hexadecimal.
	 */
	public static native String hmac_md5(String secret, String message) /*-{
		return $wnd.Crypto.HMAC($wnd.Crypto.MD5, message, secret);
	}-*/;

	/**
	 * Create a new random nonce string.
	 * @return
	 */
	public static String makeNonce() {
		char[] nonce = new char[NONCE_LENGTH];
		for (int i = 0; i < NONCE_LENGTH; ++i) {
			nonce[i] = NONCE_CHARS.charAt((int) (Math.random() * NONCE_LENGTH));
		}
		return new String(nonce);
	}

	public static void main(String[] args){
		System.out.println(md5("hello"));
		System.out.println(hmac_md5("secret", "message"));
	}
}
