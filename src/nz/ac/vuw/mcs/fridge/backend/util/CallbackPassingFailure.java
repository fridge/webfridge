package nz.ac.vuw.mcs.fridge.backend.util;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This abstract class partially implements a callback that will pass any failures (exceptions) on to another callback.
 * @author andrew
 */
public abstract class CallbackPassingFailure<T> implements AsyncCallback<T> {
	protected final AsyncCallback<?> chainedCallback;

	public CallbackPassingFailure(AsyncCallback<?> chainedCallback) {
		this.chainedCallback = chainedCallback;
	}

	@Override
	public void onFailure(Throwable caught) {
		chainedCallback.onFailure(caught);
	}
}
