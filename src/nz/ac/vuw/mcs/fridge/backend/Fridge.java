package nz.ac.vuw.mcs.fridge.backend;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import nz.ac.vuw.mcs.fridge.backend.model.User;
import nz.ac.vuw.mcs.fridge.backend.util.CallbackPassingFailure;
import nz.ac.vuw.mcs.fridge.backend.util.Crypt;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This class represents a remote fridge to which we connect using the interfridge protocol.
 * @author AndrewWalbran
 */
public abstract class Fridge {
	/**
	 * The name of the fridge.
	 */
	protected String name;

	/**
	 * Cached list of remote fridges known to this fridge.
	 * This is a map of fridge name to endpoint URL.
	 */
	private Set<String> fridges;

	/**
	 * Number of people served, cached from calling get_messages.
	 */
	private int numServed;
	
	/**
	 * Message about how to sign up for fridge, cached from calling get_messages.
	 */
	private String signupInfo;

	/**
	 * List of people who owe fridge money and should be told off, cached from calling get_messages.
	 */
	private List<String> naughtyPeople;

	public static void makeFridge(String endpoint, final AsyncCallback<Fridge> callback) {
		final Fridge fridge = new DirectFridge(endpoint);
		fridge.callGetFridgeName(new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String result) {
				fridge.name = result;
				callback.onSuccess(fridge);
			}
		});
	}

	/**
	 * Call the given method on this fridge. Concrete subclasses implement this, either talking directly to this fridge or proxying the call via our local fridge.
	 * @param <T> The return type of the XML-RPC method.
	 * @param methodName The name of the XML-RPC method to call.
	 * @param arguments Arguments to pass to the XML-RPC method.
	 * @param callback Callback to call with the results of the method call or any errors.
	 */
	protected abstract <T> void makeXmlRpcCall(String methodName, Object[] arguments, AsyncCallback<T> callback);

	/**
	 * Authenticate a user at this fridge with the given username and password. If the username and password provided are valid then return the user; else throw an InterfridgeException.
	 * @param username The username of the user at this fridge to authenticate.
	 * @param password Their password.
	 * @return An object representing the user.
	 * @throws InterfridgeException If there is an error communicating with the fridge server or authenticating the user.
	 */
	public void authenticateUser(final String username, final String password, final AsyncCallback<AuthenticatedUser> callback) {
		callGenerateNonce(username, password, new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String snonce) {
				callTransfer(snonce, username, username, 0, password, new CallbackPassingFailure<Integer>(callback) {
					@Override
					public void onSuccess(Integer balance) {
						callback.onSuccess(new AuthenticatedInterfridgeUser(Fridge.this, username, password, balance));
					}
				});
			}
		});
	}

	/**
	 * Get an object representing a user at this fridge with the given username.
	 * @param username The username of the user at this fridge.
	 * @return An object representing the user.
	 * @throws InterfridgeException If there is an error communicating with the fridge server or the user does not exist.
	 */
	public void getUser(String username, final AsyncCallback<User> callback) {
		final User user = new InterfridgeUser(this, username);
		user.getRealName(new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String result) {
				//Ignore the result, we are just calling this to force the object to initialise itself by calling a method on the fridge, to verify that they actually exist
				callback.onSuccess(user);
			}
		});
	}

	/**
	 * Get a local or remote user, by either a local username or username@fridge.
	 * @param identifier A string identifying the local or remote user, including the fridge name if appropriate.
	 * @return An object representing the user.
	 * @throws IllegalArgumentException If an invalid remote fridge name is given.
	 */
	public void parseUser(final String identifier, final AsyncCallback<User> callback) {
		final int at = identifier.indexOf('@');
		if (at == -1) {
			//Local user
			getUser(identifier, callback);
		}
		else {
			String fridgename = identifier.substring(at + 1);
			final String username = identifier.substring(0, at);
			if (fridgename.equals(name)) {
				//Fully-qualified local user
				getUser(username, callback);
			}
			getRemoteFridge(fridgename, new CallbackPassingFailure<Fridge>(callback) {	
				@Override
				public void onSuccess(Fridge fridge) {
					if (fridge == null) {
						//No such fridge
						callback.onFailure(new IllegalArgumentException("No such fridge '" + identifier.substring(at + 1) + "'"));
					}
					fridge.getUser(username, callback);
				}
			});
		}
	}

	/**
	 * Parse and authenticate a local or remote user, by either a local username or username@fridge.
	 * @param identifier A string identifying the local or remote user, including the fridge name if appropriate.
	 * @param password Their password.
	 */
	public void parseAuthenticateUser(final String identifier, final String password, final AsyncCallback<AuthenticatedUser> callback) {
		final int at = identifier.indexOf('@');
		if (at == -1) {
			//Local user
			authenticateUser(identifier, password, callback);
		}
		else {
			String fridgename = identifier.substring(at + 1);
			final String username = identifier.substring(0, at);
			if (fridgename.equals(name)) {
				//Fully-qualified local user
				authenticateUser(username, password, callback);
			}
			getRemoteFridge(fridgename, new CallbackPassingFailure<Fridge>(callback) {	
				@Override
				public void onSuccess(Fridge fridge) {
					if (fridge == null) {
						//No such fridge
						callback.onFailure(new IllegalArgumentException("No such fridge '" + identifier.substring(at + 1) + "'"));
						return;
					}
					fridge.authenticateUser(username, password, callback);
				}
			});
		}
	}

	/**
	 * Get the unique name of this fridge. This name is also used as the username for its interfridge account on all other fridges.
	 * @return The name of the fridge.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the number of servings made by this fridge, to brag about it.
	 * @return How many purchases have been made on the fridge.
	 * @throws InterfridgeException
	 */
	public void getNumServed(final AsyncCallback<Integer> callback) throws InterfridgeException {
		ensureGotMessages(new CallbackPassingFailure<Void>(callback) {
			@Override
			public void onSuccess(Void result) {
				callback.onSuccess(numServed);
			}
		});
	}

	/**
	 * Get a message about how to signup for a fridge account at this fridge.
	 * @return Said message.
	 * @throws InterfridgeException
	 */
	public void getSignupInfo(final AsyncCallback<String> callback) {
		ensureGotMessages(new CallbackPassingFailure<Void>(callback) {
			@Override
			public void onSuccess(Void result) {
				callback.onSuccess(signupInfo);
			}
		});
	}

	/**
	 * Get a list of people who owe the fridge money, and should be told off.
	 * @return A list of these people's names.
	 * @throws InterfridgeException
	 */
	public void getNaughtyPeople(final AsyncCallback<List<String>> callback) {
		ensureGotMessages(new CallbackPassingFailure<Void>(callback) {
			@Override
			public void onSuccess(Void result) {
				callback.onSuccess(naughtyPeople);
			}
		});
	}

	/**
	 * Call get_messages() to initialise a few fields with messages and statistics.
	 */
	private void ensureGotMessages(final AsyncCallback<Void> callback) {
		if (signupInfo == null) {
			refreshMessages(callback);
		}
		else {
			callback.onSuccess(null);
		}
	}

	/**
	 * Call get_messages to refresh the value that will be returned by getNumServed(), getSignupInfo() and getNaughtyPeople().
	 * This will be called automatically the first time one of those methods is called, but it may be desirable to call it manually at some later point to update the cached values.
	 */
	public void refreshMessages(final AsyncCallback<Void> callback) {
		callGetMessages(new CallbackPassingFailure<Map<String,Object>>(callback) {
			@Override
			public void onSuccess(Map<String, Object> messages) {
				numServed = (Integer) messages.get("num_served");
				signupInfo = (String) messages.get("signup_info");

				Object[] naughtyArray = (Object[]) messages.get("naughty_people");
				naughtyPeople = new ArrayList<String>(naughtyArray.length);
				for (Object el: naughtyArray) {
					naughtyPeople.add((String) el);
				}
				callback.onSuccess(null);
			}
		});
	}

	/**
	 * Initialise the fridges field if it has not already been initialised.
	 */
	private void getFridges(final AsyncCallback<Void> callback) {
		if (fridges != null) {
			callback.onSuccess(null);
		}
		else {
			callGetFridges(new CallbackPassingFailure<Map<String,String>>(callback) {
				@Override
				public void onSuccess(Map<String, String> fridgeEndpoints) {
					fridges = fridgeEndpoints.keySet();
					callback.onSuccess(null);
				}
			});
		}
	}

	/**
	 * Given the name of a remote fridge which this fridge knows about, construct and return the appropriate Fridge.
	 * @param name The name of the remote fridge.
	 * @return A Fridge object representing the requested remote fridge, or null if no such fridge is known.
	 * @throws InterfridgeException
	 */
	public void getRemoteFridge(final String name, final AsyncCallback<Fridge> callback) {
		getFridges(new CallbackPassingFailure<Void>(callback) {
			@Override
			public void onSuccess(Void result) {
				if (fridges.contains(name)) {
					//All other fridges will be proxied via this one, or via our proxy
					//ProxiedFridge's constructor will take care of tracing back the string of proxies as necessary
					callback.onSuccess(new ProxiedFridge(name, Fridge.this));
				}
				else {
					callback.onSuccess(null); //TODO: Would it be better to change the specification, and call onFailure?
				}
			}
		});
	}

	/**
	 * Get a list of remote fridge names.
	 * @return A list of the names of all the remote fridges peered with this fridge.
	 * @throws InterfridgeException
	 */
	public void getRemoteFridges(final AsyncCallback<Set<String>> callback) {
		getFridges(new CallbackPassingFailure<Void>(callback) {
			@Override
			public void onSuccess(Void result) {
				callback.onSuccess(fridges);
			}
		});
	}

	private void callGetFridges(final AsyncCallback<Map<String, String>> callback) {
		makeXmlRpcCall("get_fridges", new Object[]{}, new AsyncCallback<Object>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling get_fridges method", caught));
			}

			@Override
			public void onSuccess(Object result) {
				if (result instanceof Map) {
					callback.onSuccess((Map<String, String>) result);
				}
				else {
					callback.onSuccess(new HashMap<String, String>());
				}
			}
		});
	}

	/**
	 * Call the generate_nonce method on the fridge server to fetch a nonce for the following request.
	 * @return The nonce provided by the fridge.
	 */
	protected void callGenerateNonce(final String username, final String password, final AsyncCallback<String> callback) {
		//Generate client nonce and timestamp
		long timestamp = new Date().getTime() / 1000;
		final String cnonce = Crypt.makeNonce();
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), cnonce + "," + timestamp + "," + username);

		makeXmlRpcCall("generate_nonce", new Object[]{cnonce, (int) timestamp, username, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
				callback.onFailure(new InterfridgeException("Error calling generate_nonce method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				String snonce = (String) result.get("nonce");
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + cnonce))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on nonce response."));
					return;
				}
				callback.onSuccess(snonce);
			}
		});
	}

	/**
	 * Call the transfer method on the fridge.
	 * @return The new balance of <tt>fromUser</tt> after the successful transfer.
	 * @throws InterfridgeException If an error occurred communicating with the server, or if the server returned an error.
	 */
	protected void callTransfer(final String snonce, String fromUser, String toUser, int amount, final String password, final AsyncCallback<Integer> callback) {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + fromUser + "," + toUser + "," + amount);
		Map<String, Object> result;
		makeXmlRpcCall("transfer", new Object[]{snonce, fromUser, toUser, amount, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling transfer method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				int balance;
				try {
					balance = (Integer) result.get("balance");
				}
				catch (NullPointerException e) {
					callback.onFailure(new InterfridgeException("Balance missing in response from remote fridge to transfer method."));
					return;
				}
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on transfer response."));
					return;
				}
				callback.onSuccess(balance);
			}
		});
	}

	protected void callTopup(final String snonce, String username, int amount, final String password, final AsyncCallback<Integer> callback) {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + username + "," + amount);
		makeXmlRpcCall("topup", new Object[]{snonce, username, amount, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling topup method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				int balance;
				try {
					balance = (Integer) result.get("balance");
				}
				catch (NullPointerException e) {
					callback.onFailure(new InterfridgeException("Balance missing in response from fridge to topup method."));
					return;
				}
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on topup response."));
					return;
				}
				callback.onSuccess(balance);
			}
		});
	}

	protected void callRemoteTopup(final String snonce, String topupFridge, String username, int amount, final String password, final AsyncCallback<Integer> callback) {
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + topupFridge + "," + username + "," + amount);
		makeXmlRpcCall("remote_topup", new Object[]{snonce, topupFridge, username, amount, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling remote_topup method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				int balance;
				try {
					balance = (Integer) result.get("balance");
				}
				catch (NullPointerException e) {
					callback.onFailure(new InterfridgeException("Balance missing in response from fridge to remote_topup method."));
					return;
				}
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on remote_topup response."));
					return;
				}
				callback.onSuccess(balance);
			}
		});
	}

	protected class PurchaseResult {
		public final int balance, orderTotal;

		public PurchaseResult(int balance, int orderTotal) {
			this.balance = balance;
			this.orderTotal = orderTotal;
		}
	}

	protected void callPurchase(final String snonce, String username, List<OrderLine> order, final String password, final AsyncCallback<PurchaseResult> callback) {
		List<Map<String, Object>> items = new ArrayList<Map<String, Object>>(order.size());
		String itemsString = convertItemsForPurchase(order, items);
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + username + itemsString);
		makeXmlRpcCall("purchase", new Object[]{snonce, username, items, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling purchase method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				int balance, orderTotal;
				try {
					balance = (Integer) result.get("balance");
					orderTotal = (Integer) result.get("order_total");
				}
				catch (NullPointerException e) {
					callback.onFailure(new InterfridgeException("Balance or order total missing in response from fridge to purchase method."));
					return;
				}
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance + "," + orderTotal))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on purchase response."));
					return;
				}
				callback.onSuccess(new PurchaseResult(balance, orderTotal));
			}
		});
	}

	/**
	 * Helper method used by callPurchase and callRemotePurchase to convert the order into the necessary array and construct the string used in the HMAC.
	 * @param order The order.
	 * @param items An array into which to store the items of the order.
	 * @return A string representing all the items in the order, to be used in calculating the HMAC.
	 */
	private String convertItemsForPurchase(List<OrderLine> order, List<Map<String,Object>> items) {
		String itemsString = "";
		for (OrderLine line: order) {
			StockItem product = line.getProduct();
			itemsString += "," + product.productCode + "," + line.getQty();
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("code", product.productCode);
			item.put("quantity", line.getQty());
			items.add(item);
		}
		return itemsString;
	}

	protected void callRemotePurchase(final String snonce, String purchaseFridge, String username, List<OrderLine> order, final String password, final AsyncCallback<PurchaseResult> callback) {
		List<Map<String, Object>> items = new ArrayList<Map<String, Object>>(order.size());
		String itemsString = convertItemsForPurchase(order, items);
		String requestHmac = Crypt.hmac_md5(Crypt.md5(password), snonce + "," + purchaseFridge + "," + username + itemsString);
		makeXmlRpcCall("remote_purchase", new Object[]{snonce, purchaseFridge, username, items, requestHmac}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling remote_purchase method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				int balance, orderTotal;
				try {
					balance = (Integer) result.get("balance");
					orderTotal = (Integer) result.get("order_total");
				}
				catch (NullPointerException e) {
					callback.onFailure(new InterfridgeException("Balance or order total missing in response from fridge to remote_purchase method."));
					return;
				}
				String responseHmac = (String) result.get("hmac");
				if (!responseHmac.equals(Crypt.hmac_md5(Crypt.md5(password), snonce + "," + balance + "," + orderTotal))) {
					callback.onFailure(new InterfridgeException("Invalid HMAC on remote_purchase response."));
					return;
				}
				callback.onSuccess(new PurchaseResult(balance, orderTotal));
			}
		});
	}

	protected void callGetMessages(final AsyncCallback<Map<String, Object>> callback) {
		makeXmlRpcCall("get_messages", new Object[]{}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling get_messages method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				callback.onSuccess(result);
			}
		});
	}

	protected void callGetUserInfo(String username, final AsyncCallback<Map<String, Object>> callback) {
		makeXmlRpcCall("get_user_info", new Object[]{username}, new AsyncCallback<Map<String, Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling get_user_info method", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				callback.onSuccess(result);
			}
		});
	}

	/**
	 * Get a list of items which this fridge stocks.
	 * @param username If non-null, pass this username on to the fridge so as to get prices specific to that user. If null, give default prices.
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	protected void callGetStock(String username, final AsyncCallback<Map<String, StockItem>> callback) {
		makeXmlRpcCall("get_stock", username == null ? new Object[]{} : new Object[]{username}, new AsyncCallback<List<Map<String, Object>>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling get_stock method", caught));
			}

			@Override
			public void onSuccess(List<Map<String, Object>> result) {
				Map<String, StockItem> items = new HashMap<String, StockItem>();
				for (Map<String, Object> item: result) {
					items.put((String) item.get("product_code"), new StockItem((String) item.get("product_code"), (String) item.get("description"), (Integer) item.get("in_stock"), (Integer) item.get("price"), (String) item.get("category"), (Integer) item.get("category_order")));
				}
				callback.onSuccess(items);
			}
		});
	}

	protected void callGetFridgeName(final AsyncCallback<String> callback) {
		makeXmlRpcCall("get_fridge_name", new Object[]{}, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Error calling get_fridge_name method", caught));
			}

			@Override
			public void onSuccess(String result) {
				callback.onSuccess(result);
			}
		});
	}

	/**
	 * Get a list of items which this fridge stocks.
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	public void getStock(AsyncCallback<Map<String, StockItem>> callback) {
		callGetStock(null, callback);
	}

	/**
	 * Get a list of items which this fridge stocks, with prices &c. for the given (local or remote) user, taking into account their account type when calculating prices.
	 * If the user is remote then their fridge name is used.
	 * @param user
	 * @return Said list of stocked items.
	 * @throws InterfridgeException If an error occurred communicating with the server.
	 */
	public void getStockForUser(User user, AsyncCallback<Map<String, StockItem>> callback) {
		//TODO: Method User.getLocalUsernameForFridge to do this?
		if (user.getFridge().equals(this)) {
			callGetStock(user.getLocalUsername(), callback);
		}
		else {
			callGetStock(user.getFridge().getName(), callback);
		}
	}

	/**
	 * Given an integer amount of money in cents, return a formatted string suitable for display.
	 * @param amount Amount in cents.
	 * @return Formatted price string.
	 */
	public static String formatMoney(int amount) {
		String sign;
		if (amount < 0) {
			amount = -amount;
			sign = "-";
		}
		else {
			sign = "";
		}
		int cents = amount % 100;
		return sign + "$" + (amount / 100) + "." + (cents < 10 ? "0" : "") + cents;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Fridge && ((Fridge) other).name.equals(name);
	}
}
