package nz.ac.vuw.mcs.fridge.backend;

import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.model.User;
import nz.ac.vuw.mcs.fridge.backend.util.CallbackPassingFailure;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class InterfridgeUser implements User {
	protected final Fridge fridge;
	protected final String username;

	//These are only initialised when first they are needed. Subclasses should thus use the accessor methods.
	private String realname;
	private Boolean isadmin;

	public InterfridgeUser(Fridge fridge, String username) {
		if (fridge == null) {
			throw new IllegalArgumentException("InterfridgeUser may not have null fridge.");
		}
		else if (username == null) {
			throw new IllegalArgumentException("InterfridgeUser may not have null username.");
		}
		this.fridge = fridge;
		this.username = username;
	}

	public String getUsername() {
		return username + "@" + fridge.getName();
	}

	/**
	 * Helper method to initialise a local cache of various information about this user by calling get_user_info on the fridge server.
	 */
	private void initialiseInfo(final AsyncCallback<Void> callback) {
		fridge.callGetUserInfo(username, new AsyncCallback<Map<String,Object>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(new InterfridgeException("Bad return value from get_user_info", caught));
			}

			@Override
			public void onSuccess(Map<String, Object> result) {
				isadmin = (Boolean) result.get("isadmin");
				realname = (String) result.get("real_name");
				callback.onSuccess(null);
			}
		});
	}

	public void getRealName(final AsyncCallback<String> callback) {
		if (realname != null) {
			callback.onSuccess(realname);
		}
		else {
			initialiseInfo(new CallbackPassingFailure<Void>(callback) {
				@Override
				public void onSuccess(Void result) {
					//result means nothing and will be null; initialiseInfo will set realname
					callback.onSuccess(realname);
				}
			});
		}
	}

	public void isAdmin(final AsyncCallback<Boolean> callback) {
		if (isadmin != null) {
			callback.onSuccess(isadmin);
		}
		else {
			initialiseInfo(new CallbackPassingFailure<Void>(callback) {
				@Override
				public void onSuccess(Void result) {
					//result means nothing and will be null; initialiseInfo will set isadmin
					callback.onSuccess(isadmin);
				}
			});
		}
	}

	public Fridge getFridge() {
		return fridge;
	}

	public String getLocalUsername() {
		return username;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof InterfridgeUser && ((InterfridgeUser) other).fridge.equals(fridge) && ((InterfridgeUser) other).username.equals(username);
	}
}
