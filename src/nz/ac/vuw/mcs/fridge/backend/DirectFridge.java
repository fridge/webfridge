package nz.ac.vuw.mcs.fridge.backend;

import com.fredhat.gwt.xmlrpc.client.XmlRpcClient;
import com.fredhat.gwt.xmlrpc.client.XmlRpcRequest;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class DirectFridge extends Fridge {
	/**
	 * The HTTP endpoint URL used to connect to the remote fridge.
	 */
	private final String endpoint;

	private final XmlRpcClient xmlrpc;

	public DirectFridge(String name, String endpoint) {
		if (name == null || endpoint == null) {
			throw new NullPointerException();
		}
		this.name = name;
		this.endpoint = endpoint;

		xmlrpc = new XmlRpcClient(endpoint);
		//xmlrpc.setDebugMode(true);
	}

	/**
	 * Note that this does not set the <tt>name</tt> field, and so the resulting fridge should not be used until this is set.
	 */
	protected DirectFridge(String endpoint) {
		if (endpoint == null) {
			throw new NullPointerException();
		}
		this.endpoint = endpoint;
		xmlrpc = new XmlRpcClient(endpoint);
		//xmlrpc.setDebugMode(true);
	}

	protected <T> void makeXmlRpcCall(String methodName, Object[] arguments, AsyncCallback<T> callback) {
		new XmlRpcRequest<T>(xmlrpc, methodName, arguments, callback).execute();
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof DirectFridge && ((DirectFridge) other).endpoint.equals(endpoint);
	}
}
