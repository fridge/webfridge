package nz.ac.vuw.mcs.fridge.backend;

import java.util.List;
import java.util.Vector;

import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.FridgeTransaction;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import nz.ac.vuw.mcs.fridge.backend.model.User;
import nz.ac.vuw.mcs.fridge.backend.util.CallbackPassingFailure;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class AuthenticatedInterfridgeUser extends InterfridgeUser implements AuthenticatedUser {
	//We need to keep the password around because it is needed to make purchases
	private final String password;
	private int balance;

	public AuthenticatedInterfridgeUser(Fridge fridge, String username, String password, int balance) {
		super(fridge, username);
		this.password = password;
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public List<FridgeTransaction> getTransactionFeed() {
		//No transactions for interfridge users
		return new Vector<FridgeTransaction>();
	}

	public void purchase(final Fridge purchaseFridge, final List<OrderLine> order, final AsyncCallback<Integer> callback) {
		fridge.callGenerateNonce(username, password, new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String snonce) {
				AsyncCallback<Fridge.PurchaseResult> continuation = new CallbackPassingFailure<Fridge.PurchaseResult>(callback) {
					@Override
					public void onSuccess(Fridge.PurchaseResult result) {
						balance = result.balance;
						callback.onSuccess(result.orderTotal);
					}
				};
				if (purchaseFridge.equals(fridge)) {
					fridge.callPurchase(snonce, username, order, password, continuation);
				}
				else {
					fridge.callRemotePurchase(snonce, purchaseFridge.getName(), username, order, password, continuation);
				}
			}
		});
	}

	public void topup(final Fridge topupFridge, final int amount, final AsyncCallback<Void> callback) {
		fridge.callGenerateNonce(username, password, new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String snonce) {
				AsyncCallback<Integer> continuation = new CallbackPassingFailure<Integer>(callback) {
					@Override
					public void onSuccess(Integer result) {
						balance = result;
						callback.onSuccess(null);
					}
				};
				if (topupFridge.equals(fridge)) {
					fridge.callTopup(snonce, username, amount, password, continuation);
				}
				else {
					fridge.callRemoteTopup(snonce, topupFridge.getName(), username, amount, password, continuation);
				}
			}
		});
	}

	public void transfer(final User toUser, final int amount, final AsyncCallback<Void> callback) {
		fridge.callGenerateNonce(username, password, new CallbackPassingFailure<String>(callback) {
			@Override
			public void onSuccess(String snonce) {
				String toUsername;
				if (toUser.getFridge().equals(fridge)) {
					//Local user
					toUsername = toUser.getLocalUsername();
				}
				else {
					toUsername = toUser.getUsername();
				}
				fridge.callTransfer(snonce, username, toUsername, amount, password, new CallbackPassingFailure<Integer>(callback) {
					@Override
					public void onSuccess(Integer result) {
						balance = result;
					}
				});
			}
		});
	}
}
