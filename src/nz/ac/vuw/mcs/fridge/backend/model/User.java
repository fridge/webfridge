package nz.ac.vuw.mcs.fridge.backend.model;

import com.google.gwt.user.client.rpc.AsyncCallback;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

public interface User {
	/**
	 * Get this user's fully-qualified username, in the form "username@fridge".
	 * @return
	 */
	public String getUsername();

	/**
	 * Get just the actual username of this user, not including the "@fridge" bit.
	 */
	public String getLocalUsername();

	/**
	 * Get this user's home fridge.
	 */
	public Fridge getFridge();

	public void getRealName(AsyncCallback<String> callback);

	public void isAdmin(AsyncCallback<Boolean> callback);
}
