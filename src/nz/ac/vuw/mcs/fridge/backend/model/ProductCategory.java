package nz.ac.vuw.mcs.fridge.backend.model;

public class ProductCategory implements Comparable<ProductCategory> {
	private int id;
	private String title;

	public ProductCategory(int newID, String newTitle) {
		id = newID;
		title = newTitle;
	}

	public int getID() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof ProductCategory && id == ((ProductCategory) other).id;
	}

	public int compareTo(ProductCategory other) {
		return id - other.id;
	}

	@Override
	public String toString() {
		return title + "(" + id + ")";
	}

	@Override
	public int hashCode() {
		return id;
	}
}
