/*
 * Created on Mar 26, 2005
 */
package nz.ac.vuw.mcs.fridge.backend.model;

/**
 * @author neil
 */
public class OrderLine {
	private final StockItem product;
	private int quantity;

	public OrderLine(StockItem product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public int getQty() {
		return quantity;
	}

	public void incrementQty(int by) {
		quantity += by;
	}

	public void setQty(int quantity) {
		this.quantity = quantity;
	}

	public StockItem getProduct() {
		return product;
	}
}
