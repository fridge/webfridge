package nz.ac.vuw.mcs.fridge.backend.model;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;

public interface AuthenticatedUser extends User {
	/**
	 * Get this user's current balance.
	 * @return The user's current balance, in cents.
	 */
	public int getBalance();

	/**
	 * Gets the last 15 transactions for the current user.
	 * @return List of transactions
	 */
	public List<FridgeTransaction> getTransactionFeed();

	/**
	 * Transfer the given amount of money from this user to <code>toUser</code>.
	 * As a side-effect, this will update the locally cached value of the user's balance.
	 * @param toUser The user to whom to transfer the money.
	 * @param amount The amount of money to transfer, in cents. This must be positive.
	 * @throws InterfridgeException 
	 */
	public void transfer(User toUser, int amount, AsyncCallback<Void> callback);

	/**
	 * Make a purchase at the given fridge.
	 * As a side-effect, this will update the locally cached value of the user's balance.
	 * @param purchaseFridge The fridge at which to make the purchase.
	 * @param order The items to order.
	 * @return The total price of the order.
	 * @throws InterfridgeException If an error occurred communicating with the server, or if the server returned an error.
	 */
	public void purchase(Fridge purchaseFridge, List<OrderLine> order, AsyncCallback<Integer> callback);

	/**
	 * Topup this user's fridge account by the given amount, depositing the cash at the given (home or remote) fridge.
	 * @param topupFridge The fridge at which the cash is deposited.
	 * @param amount The amount of money to add to the account, in cents. This may be positive or negative.
	 * @throws InterfridgeException If an error occurred communicating with the server, or if the server returned an error.
	 */
	public void topup(Fridge topupFridge, int amount, AsyncCallback<Void> callback);
}
