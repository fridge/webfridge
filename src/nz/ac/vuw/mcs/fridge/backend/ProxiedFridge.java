package nz.ac.vuw.mcs.fridge.backend;

import java.util.Arrays;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class ProxiedFridge extends Fridge {
	private final DirectFridge proxyVia;

	/**
	 * Construct a new fridge whose XML-RPC calls will be proxied via the given (presumably local) fridge.
	 * @param name The name of the new fridge. This name must be recognised via the fridge doing the proxying, i.e. it must be peered.
	 * @param proxyVia Fridge via whom to make XML-RPC method calls. This will be traced back to a direct fridge.
	 */
	public ProxiedFridge(String name, Fridge proxyVia) {
		this.name = name;
		//In case we are trying to proxy via another proxied fridge, trace back the chain until we get to a direct fridge
		while (proxyVia instanceof ProxiedFridge) {
			proxyVia = ((ProxiedFridge) proxyVia).proxyVia;
		}
		this.proxyVia = (DirectFridge) proxyVia;
	}

	@Override
	protected <T> void makeXmlRpcCall(String methodName, Object[] arguments, AsyncCallback<T> callback) {
		//Ask the local fridge to proxy the method call for us, giving it the name of this fridge, method name and arguments
		proxyVia.makeXmlRpcCall("proxy", new Object[]{name, methodName, Arrays.asList(arguments)}, callback);
	}
}
